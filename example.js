

// Generating PDF with selected page sections on the go (Accomodation view, removing street number, )

const genpdf = () => {
		
		const regExp =  /^\d+\-|\d+/g
		const streetSrc = document.querySelector('.superhide') ||false 
		let streetchange = streetSrc.textContent.replace(regExp, "");
		streetSrc.textContent = streetchange;
				
		let justAPlaceholder = document.querySelector('.pdf-former');
		let contentoContainer = document.createElement("div");
		contentoContainer.classList.add('mypdf');
		
		// gather data - select page elements
		let title = document.querySelector ('h1.entry-title').textContent;
		let description = document.querySelector('#listing_description .panel-title-description').textContent;
		let images = document.querySelector(".carousel-inner");
		let descps = document.querySelectorAll("#listing_description_content >p");
		let prices = document.querySelector('#listing_price');
		let details = document.querySelector('#listing_details').parentNode;
		let amenities = document.querySelector('#listing_ammenities').parentNode;
		let address = document.querySelector('#collapseTwo').parentNode;
		let sleepArr = document.querySelector('a[data-parent="#accordion_prop_sleepibg"]').parentNode;
		let termsCond = document.querySelector('a[data-parent="#accordion_prop_terns"]').parentNode;
		
		
		
		// generate view - de-constructed for easier read
		contentoContainer.insertAdjacentHTML('beforeend', '<h3>Property Pictures</h3>');
		contentoContainer.insertAdjacentHTML('beforeend', images.innerHTML);
		contentoContainer.insertAdjacentHTML('beforeend','<div class="html2pdf__page-break"></div>');
		contentoContainer.insertAdjacentHTML('beforeend', '<br>');
		contentoContainer.insertAdjacentHTML('beforeend', '<h3>Property Description</h3>');
		for (let descp of descps) {
			contentoContainer.insertAdjacentHTML('beforeend', `<p>${descp.textContent}</p>`)
		}
		contentoContainer.insertAdjacentHTML('beforeend','<hr>');
		contentoContainer.insertAdjacentHTML('beforeend', address.innerHTML);
		contentoContainer.insertAdjacentHTML('beforeend', '<br>');
		contentoContainer.insertAdjacentHTML('beforeend','<hr>');
		contentoContainer.insertAdjacentHTML('beforeend', sleepArr.innerHTML);
		contentoContainer.insertAdjacentHTML('beforeend', '<br>');
		contentoContainer.insertAdjacentHTML('beforeend','<hr>');
		contentoContainer.insertAdjacentHTML('beforeend', details.innerHTML);
		contentoContainer.insertAdjacentHTML('beforeend','<hr class="html2pdf__page-break">');
		contentoContainer.insertAdjacentHTML('beforeend', amenities.innerHTML);
		contentoContainer.insertAdjacentHTML('beforeend', '<br>');
		contentoContainer.insertAdjacentHTML('beforeend','<hr>');
		contentoContainer.insertAdjacentHTML('beforeend', termsCond.innerHTML);
		
		let opt = {
			filename: 'accomodation.pdf'
		}
		html2pdf().set(opt).from(contentoContainer).save();
};

// Working on php-generated calendar view. Helper for styling start and end booking dates
$('#chin').datepicker();
$('#chout').datepicker();
let chstart = document.querySelector('#start_date')
let chend = document.querySelector('#end_date')
let chout = document.querySelector('#chout');



if (chstart.value.match(/^\d/)) {
	chin.value = chstart.value;
}
if (chend.value.match(/^\d/)) {
	chout.value = chend.value;
}

$('#chout').on('change', function() {
	let chinDate = chin.value;
chinDate = chinDate.split('-');
let newChinDate = new Date (chinDate[2], chinDate[1] - 1, chinDate[0]);
let choutDate = chout.value;
choutDate = choutDate.split('-');
let newChoutDate = new Date (choutDate[2], choutDate[1] -1, choutDate[0]);
	if ((newChoutDate - newChinDate) / 1000 < 86400 * 28 ) {
		$('.aldiv').show('slow');
		} else {
				$('.aldiv').hide('slow');
		}		
				chstart.value = chin.value;
				chend.value = chout.value;
		
})
$('#chin').on('change', function() {
	let chinDate = chin.value;
chinDate = chinDate.split('-');
let newChinDate = new Date (chinDate[2], chinDate[1] - 1, chinDate[0]);
let choutDate = chout.value;
choutDate = choutDate.split('-');
let newChoutDate = new Date (choutDate[2], choutDate[1] -1, choutDate[0]);
	if ((newChoutDate - newChinDate) / 1000 < 86400 * 28 ) {
		$('.aldiv').show('slow');
		} else {
				$('.aldiv').hide('slow');
		}		
				chstart.value = chin.value;
				chend.value = chout.value;
		
})
